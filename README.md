# Mookie

Mookie ist eine anpassbare Cookie-Meldung für die gewöhnliche Weitergabe der Information, dass Cookies auf einer Seite verwendet werden oder um dem Nutzer die Möglichkeit zu bieten sich gegen die Verwendung seiner Daten auszusprechen.

## Wie wird Mookie eingesetzt?

Zunächst wird `mookie.css` und `mookie.js` in der genannten Reihenfolge in die Seite eingebunden. Werden Remarketing-Tools bzw. Analyse-Tools eingesetzt, so muss die Einbettung vor der Initialisierung der jeweiligen Tools erfolgen. Aktuell wird nur Google Analytics (Tag Manager) unterstützt.