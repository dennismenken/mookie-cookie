var mookie = {
    // Default parameters
    defaults: {
        privacyLink: '',
        overlay: true,
        position: 'top',
        gaProperty: '',
        cookie: 'mookieOptOut',
        optButtonClass: 'oioo-button',
        debug: false,
        language: 'de'
    },
    lang: {
        de: {
            btnAccept: 'Einverstanden',
            btnDecline: 'Analytics deaktivieren',
            btnEnable: 'Analytics aktivieren',
            btnDisable: 'Analytics deaktivieren',
            message: 'Unsere Webseite verwendet Cookies, die dabei helfen unsere Webseite zu verbessern und unseren Nutzern die bestmöglichste Erfahrung zu bieten.',
            analytics: 'Um Ihnen ein erfreuliches Erlebnis auf unserer Webseite ermöglichen zu können, setzen wir Cookies und Web-Analyse ein. Detaillierte Informationen und wie Sie der Verwendung jederzeit widersprechen können, finden Sie in unseren Datenschutzhinweisen.',
            privacyLink: 'Mehr erfahren &raquo;'
        },
        en: {
            btnAccept: 'Accept',
            btnDecline: 'Disable analytics',
            btnEnable: 'Enable analytics',
            btnDisable: 'Disable analytics',
            message: 'This website uses cookies to ensure you get the best experience on our website.',
            analytics: 'This website uses cookies and web analysis to ensure you get the best experience on our website. You will find detailed information in our privacy policy.',
            privacyLink: 'Learn more &raquo;'
        }
    },

	// Empty variables
	gaProperty: '',
	gaDisableProperty: '',
    hasAnalytics: false,

    // ES6: init: function (args = {}, lang = {}) {
    init: function (args, lang) {
    	args = (args === undefined ? {} : args);
        lang = (lang === undefined ? {} : lang);
        // We have to merge the default parameters before processing.
        Object.assign(this.defaults, args);
        // Check if the given language exists. Otherwise add the property to current object.
        if(!this.lang.hasOwnProperty(this.defaults.language)) this.lang[this.defaults.language] = {};
        Object.assign(this.lang[this.defaults.language], lang);
    	// We have to check if the privacy link has been set. Otherwise we have to return false and stop processing.
    	if(!this.defaults.privacyLink) return false;
    	// We have to check if the Analytics property has been set. If set, we have to modify some variables.
    	if(this.defaults.gaProperty) {
    	    this.hasAnalytics = true;
            this.gaDisableProperty = 'ga-disable-' + this.defaults.gaProperty;
        }

        if (document.cookie.indexOf(mookie.defaults.cookie + '=2') > -1) {
            window['ga-disable-' + mookie.defaults.gaProperty] = true;
        }

        // Get the current cookie
		var c = util.getCookie(this.defaults.cookie);

		if (c == 1 && !this.hasAnalytics) {
			if (this.defaults.debug) console.log('There is a default cookie.');
			// TODO: Hide all opt-elements
			this.optIterate(1);
		} else if (c == 2 && this.hasAnalytics) {
			if (this.defaults.debug) console.log('There is a extended cookie to disable google analytics. Set ' + this.gaDisableProperty + ' to true.');
			// TODO: We have to change the ability of each opt-element.
			this.optIterate(2);
		} else if (c == 3 && this.hasAnalytics) {
			if (this.defaults.debug) console.log('There is a extended cookie to enable google analytics. Set ' + this.gaDisableProperty + ' to false.');
			// TODO: We have to change the ability of each opt-element.
			this.optIterate(3);
		} else if (c != "") {
			if (this.defaults.debug) console.log('The cookie is not empty, but has undefined paramters.');
			util.deleteCookie(this.defaults.cookie);
			this.message();
			if (this.hasAnalytics) {
				this.optIterate(3);
			} else {
				this.optIterate(1);
			}
		} else {
			if (this.defaults.debug) console.log('No cookie is set. Show message.');
			if (this.hasAnalytics) {
				this.optIterate(3);
			} else {
				this.optIterate(1);
			}
			this.message();
		}
        this.optListener();
	},

	message: function() {
		var messageElement = document.createElement('div');
		messageElement.className = 'mookie-container';
		messageElement.setAttribute('id', 'mookie');
		messageElement.classList.add('mookie-' + this.defaults.position);
		if (this.defaults.overlay) {
			messageElement.classList.add('mookie-overlay');
		} else {
			messageElement.classList.add('mookie-static');
		}

		var mElementHTML = '<div class="mookie">' +
			'<div class="mookie-column mookie-text">' +
			'<p>' + ((this.hasAnalytics) ? this.lang[this.defaults.language].analytics : this.lang[this.defaults.language].message) + ' <a href="' + this.defaults.privacyLink + '">' + this.lang[this.defaults.language].privacyLink + '</a></p>' +
			'</div>' +
			'<div class="mookie-column mookie-actions">' +
			'<ul class="mookie-buttons">' +
			((this.hasAnalytics) ? '<li><a href="#" class="mookie-button mookie-decline" onclick="mookie.confirm(event, 2)">' + this.lang[this.defaults.language].btnDecline + '</a></li>' : '') +
			'<li><a href="#" class="mookie-button mookie-accept" onclick="mookie.confirm(event, ' + ((this.hasAnalytics) ? 3 : 1) + ')">' + this.lang[this.defaults.language].btnAccept + '</a></li>' +
			'</ul>' +
			'</div>' +
			'</div>';

		messageElement.innerHTML = mElementHTML;

		document.body.insertBefore(messageElement, document.body.firstChild);
	},

	optToggle: function() {
		var c = util.getCookie(this.defaults.cookie);
		if (c == 2) {
			if (this.defaults.debug) console.log('Change value to false');
			util.setCookie(this.defaults.cookie, 3, 180);
			window[this.gaDisableProperty] = false;
			// TODO: We have to change the ability of each opt-element.
			this.optIterate(3);
		} else if (c == 3) {
			if (this.defaults.debug) console.log('Change value to true.');
			util.setCookie(this.defaults.cookie, 2, 180);
			window[this.gaDisableProperty] = true;
			// TODO: We have to change the ability of each opt-element.
			this.optIterate(2);
		} else {
			if (this.defaults.debug) console.log('I am the else candidate.');
			util.setCookie(this.defaults.cookie, 2, 60);
			window[this.gaDisableProperty] = true;
			// TODO: We have to change the ability of each opt-element.
			this.optIterate(3);
		}
	},

	optIterate: function(state) {
		var el = document.getElementsByClassName(this.defaults.optButtonClass);
		var i;
		for (i = 0; i < el.length; i++) {
			switch (state) {
				case 3:
					el[i].innerHTML = this.lang[this.defaults.language].btnDisable;
					if (el[i].classList.contains('mookie-opt-in')) el[i].classList.remove('mookie-opt-in');
					el[i].classList.add('mookie-opt-out');
                    if (this.defaults.debug) console.log('case 3');
					break;
				case 2:
					el[i].innerHTML = this.lang[this.defaults.language].btnEnable;
					if (el[i].classList.contains('mookie-opt-out')) el[i].classList.remove('mookie-opt-out');
					el[i].classList.add('mookie-opt-in');
                    if (this.defaults.debug) console.log('case 2');
					break;
				case 1:
					el[i].style.display = 'none';
                    if (this.defaults.debug) console.log('case 1');
					break;
				default:
					el[i].innerHTML = this.lang[this.defaults.language].btnDisable;
					if (el[i].classList.contains('mookie-opt-in')) el[i].classList.remove('mookie-opt-in');
					el[i].classList.add('mookie-opt-out');
                    if (this.defaults.debug) console.log('default');
			}
		}
	},

    optListener: function() {
        var el = document.getElementsByClassName(this.defaults.optButtonClass);
        for (i = 0; i < el.length; i++) {
            el[i].addEventListener('click', function(e) {
                e.preventDefault();
                mookie.optToggle();
            });
        }
    },

	confirm: function(e, state) {
		e.preventDefault();
		util.setCookie(this.defaults.cookie, state, 180);
		if (state == 2) {
			window[this.gaDisableProperty] = true;
		}
		// TODO: We have to change the ability of each opt-element.
		this.optIterate(state);
		// TODO: Hide message
		var mEl = document.getElementById('mookie');
		if (this.defaults.position == 'top') {
			mEl.classList.remove('mookie-top');
			mEl.classList.add('mookie-top-back');
		} else if (this.defaults.position == 'bottom') {
			mEl.classList.remove('mookie-bottom');
			mEl.classList.add('mookie-bottom-back');
		}
	}

};

var util = {
	setCookie: function(cname, cvalue, exdays) {
		var d = new Date();
		d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
		var expires = "expires=" + d.toUTCString();
		document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
	},
	getCookie: function(cname) {
		var name = cname + "=";
		var ca = document.cookie.split(';');
		for (var i = 0; i < ca.length; i++) {
			var c = ca[i];
			while (c.charAt(0) == ' ') {
				c = c.substring(1);
			}
			if (c.indexOf(name) == 0) {
				return c.substring(name.length, c.length);
			}
		}
		return "";
	},
	deleteCookie: function(cname) {
		setCookie(cname, '', -1);
	}
};